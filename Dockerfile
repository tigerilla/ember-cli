FROM node:4
MAINTAINER RxOnline <info@prescriptionrex.com>
RUN mkdir /ember-app
RUN apt-get update && apt-get install -y \
    build-essential \
    && npm config set loglevel warn \
    && npm install -g ember-cli \
    && npm install -g bower \
    && npm install -g phantomjs-prebuilt \
    && git clone https://github.com/facebook/watchman.git \
    && cd watchman \
    && git checkout v3.1 \
    && ./autogen.sh \
    && ./configure \
    && make \
    && make install \
    && rm -rf /watchman \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
WORKDIR /ember-app
COPY . .
EXPOSE 4200
CMD ember serve